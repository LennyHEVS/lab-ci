#include "avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(2, integer_avg(0, 4), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(2, integer_avg(4, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-4, 4), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(-3, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-3, integer_avg(-2, -4), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(12, integer_avg(10, 15), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-25, integer_avg(100, -150), "Error in integer_avg");
}

